//
//  PrimaryViewController.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 1/27/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import UIKit

class PrimaryViewController: UIViewController {
    
    @IBOutlet weak var hoursRunLabel: UILabel!
    @IBOutlet weak var calcCostLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if !(NSUserDefaults.standardUserDefaults().boolForKey("onboarded")) {
//            APIController.sharedInstance.getPIN({ (pin, authcode, error) -> Void in
//                self.performSegueWithIdentifier("ShowWizard", sender: self)
//            })
//        }
        
        if let credentials = APIController.sharedInstance.loadCredentialsFromDefaults() {
            print(credentials)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func pinTapped(sender: UIButton) {
        APIController.sharedInstance.getPIN { (pin, authcode, error) -> Void in
            
            self.performSegueWithIdentifier("ShowWizard", sender: sender)
        }
    }
    @IBAction func refreshTokensTapped(sender: AnyObject) {
        APIController.sharedInstance.refreshTokens()
    }
    
    @IBAction func thermostatTapped(sender: AnyObject) {
        APIController.sharedInstance.requestThermostat()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print(segue.identifier)
        if segue.identifier == "ShowWizard" {
            let destinationController = segue.destinationViewController as! PINAccessViewController
            destinationController.pinString = APIController.sharedInstance.pin
            destinationController.authString = APIController.sharedInstance.authCode
        }
    }
    
    
    
    @IBAction func unwindToVC(segue: UIStoryboardSegue) {
        
    }
    

}


