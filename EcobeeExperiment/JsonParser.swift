//
//  JsonParser.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 2/2/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import Foundation
class JsonParser {
    
    func parseJSON(data: NSData?) -> [String: AnyObject]? {
        do {
            if let data = data,
                json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject] {
                    return json
            }
        } catch {
            print ("couldn't parse JSON")
        }
        
        return nil
    }
    
    class func encodeJSON(item: AnyObject?) -> NSData? {
        do {
            guard let item = item else {
                return nil
            }
            
            return try NSJSONSerialization.dataWithJSONObject(item, options: [])
        } catch {
            return nil
        }
    }
}