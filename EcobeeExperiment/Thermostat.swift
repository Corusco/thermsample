//
//  Thermostat.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 3/22/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import Foundation

class Thermostat {
    var identifier = ""
    var name = ""
    var thermostatRevision = ""
    var isRegistered = false
    var modelNumber = ""
    var serialNumber = ""
    var lastModified = ""
    var settings = [String:String]()
    
    var status = (code: "", message: "")
    
    
    func saveAsDictionary() {
        
    }
    
    func loadThermostatFromStorage() {
        
    }
    
}