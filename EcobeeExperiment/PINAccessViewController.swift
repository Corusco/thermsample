//
//  PINAccessViewController.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 2/8/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import UIKit

class PINAccessViewController: UIViewController {
   
    @IBOutlet weak var pinLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    weak var subPinLabel: UILabel!
    var pinString: String!
    var authString: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let pinString = pinString {
            pinLabel.text = pinString
            idLabel.text = authString
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func nextButtonTapped(sender: AnyObject) {
        APIController.sharedInstance.requestTokens { (auth, refresh, error) -> Void in
            if let error = error {
                print(error)
            } else {
                
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
