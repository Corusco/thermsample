//
//  APIController.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 1/27/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import Foundation
import UIKit

class APIController {
    
    let kClientID = NSURLQueryItem (name: "client_id", value: "kFjrKrJhLFk9c5h3H8rBCIL1pT3Kh11y")
    let baseURLString = "api.ecobee.com"
    var authCode = String?()
    var pin = String?()
    var accessToken = String?()
    var refreshToken = String?()
    var credentials = [String:String]?()
    var thermostatOne = Thermostat()
    private let kCredKey = "credentialsKey"
    
    static let sharedInstance = APIController()
    private init() {}
    
    func getPIN(completionHandler: (pin: String?, authcode: String?, error: String?) -> Void){
        let components = NSURLComponents()
        let kScope = NSURLQueryItem (name: "scope", value: "smartRead")
        let kResponseQuery = NSURLQueryItem (name: "response_type", value: "ecobeePin")
        components.scheme = "https"
        components.host = baseURLString
        components.queryItems = [kResponseQuery, kClientID, kScope]
        components.path = "/authorize"
        print(components.URL)
        
        let parser = JsonParser()
        
        if let url = components.URL {
            
            let dataTask = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
                
                if let error = error {
                    completionHandler(pin: nil, authcode: nil, error:"Error: \(error.localizedDescription): \(error.userInfo)")
                    return
                }
                
                guard let data = data else {
                    completionHandler(pin: nil, authcode: nil, error: "Invalid or Missing Data")
                    return
                }
                
                if let json = parser.parseJSON(data) {
                    print(json)
                    self.authCode = (json["code"] as? String)!
                    print ("Authcode is \(self.authCode)")
                    self.pin = (json["ecobeePin"] as? String)!
                    print ("Pin is \(self.pin)")
                    
                    completionHandler(pin: self.pin, authcode: self.authCode, error: nil)
                }
                
            })
            
            dataTask.resume()
        }
    }
    
    func requestTokens(completionHandler: (auth: String?, refresh: String?, error: String?) -> Void) {
        guard let authCode = authCode else{
            print("Missing Authcode")
            return
        }
        let authQuery = NSURLQueryItem (name: "code", value: authCode)
        let grantQuery = NSURLQueryItem (name: "grant_type", value: "ecobeePin")
        let components = NSURLComponents()
        components.scheme = "https"
        components.host = baseURLString
        components.queryItems = [grantQuery, authQuery, kClientID]
        components.path = "/token"
        print(components.URL)
        
        let parser = JsonParser()
        
        let uploadRequest = NSMutableURLRequest(URL: components.URL!)
        uploadRequest.HTTPMethod = "POST"
        
        let dataTask = NSURLSession.sharedSession().dataTaskWithRequest(uploadRequest) { (data, response, error) -> Void in
            
            print(data)
            print(response)
            print(error)
            
            if let error = error {
                print("Error: \(error.localizedDescription): \(error.userInfo)")
                return
            }
            
            if let httpResponse = response as? NSHTTPURLResponse {
                switch httpResponse.statusCode {
                case 200...299:
                    print("the right thing")
                case 401:
                    print("error thing")
                    //return error through handler to show user waiting to authorize
                default:
                    print("do some other thing")
                    //add support for other errors
                }
                
            }
            
            guard let data = data else {
                return
            }
            
            if let json = parser.parseJSON(data) {
                
                if let _ = json["error"] {
                    print(json)
                } else if let _ = json["refresh_token"] {
                    self.credentials = ["refreshToken":json["refresh_token"] as! String, "accessToken":json["access_token"] as! String]
                    self.saveCredentialsToDefaults()
                }
                
                return
            }
        }
        
        dataTask.resume()
        
    }
    
    func saveCredentialsToDefaults() {
        NSUserDefaults.standardUserDefaults().setObject(credentials, forKey: kCredKey)
    }
    
    func loadCredentialsFromDefaults() -> [String: String]? {
        if let credentialsFromDefaults = NSUserDefaults.standardUserDefaults().objectForKey(kCredKey) as? Dictionary<String, String>  {
            credentials = credentialsFromDefaults
            return credentialsFromDefaults
        } else {
            return nil
        }
        
    }
    
    func refreshTokens() {
        //TODO: refresh tokens periodically
        let grantQuery = NSURLQueryItem (name: "grant_type", value: "refresh_token")
        let refreshQuery = NSURLQueryItem (name: "refresh_token", value: credentials!["refreshToken"])
        
        let components = NSURLComponents()
        components.scheme = "https"
        components.host = baseURLString
        components.queryItems = [grantQuery, refreshQuery, kClientID]
        components.path = "/token"
        print(components.URL)
        
        let parser = JsonParser()
        
        let uploadRequest = NSMutableURLRequest(URL: components.URL!)
        uploadRequest.HTTPMethod = "POST"
        
        let dataTask = NSURLSession.sharedSession().dataTaskWithRequest(uploadRequest) { (data, response, error) -> Void in
            
            if let error = error {
                print("Error: \(error.localizedDescription): \(error.userInfo)")
                return
            }
            
            if let httpResponse = response as? NSHTTPURLResponse {
                switch httpResponse.statusCode {
                case 200...299:
                    print("the right thing")
                case 401:
                    print("error thing")
                    //return error through handler to show user waiting to authorize
                default:
                    print("do some other thing")
                    //add support for other errors
                }
                
            }
            
            guard let data = data else {
                return
            }
            
            if let json = parser.parseJSON(data) {
                
                if let _ = json["error"] {
                    print(json)
                } else if let _ = json["refresh_token"] {
                    self.credentials = ["refreshToken":json["refresh_token"] as! String, "accessToken":json["access_token"] as! String]
                    print(self.credentials)
                    self.saveCredentialsToDefaults()
                }
                
                return
            }
        }
        
        dataTask.resume()
        
        
    }
    
    func requestThermostat() {
        let itemsDict = ["includeAlerts":"true","selectionType":"registered","selectionMatch":"","includeEvents":"true","includeSettings":"true","includeRuntime":"true"]
        let selectionDict = ["selection":itemsDict]
        let encodedSelectionDict = JsonParser.encodeJSON(selectionDict)
        let jsonQuery = NSURLQueryItem(name: "json", value: (String(data: encodedSelectionDict!, encoding: NSUTF8StringEncoding)))
        
        let components = NSURLComponents()
        components.scheme = "https"
        components.host = baseURLString
        components.path = "/1/thermostat"
        components.queryItems = [jsonQuery]
        print(components.URL)
        
        let thermostatRequest = NSMutableURLRequest(URL:components.URL!)
        thermostatRequest.HTTPMethod = "GET"
        thermostatRequest.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        thermostatRequest.setValue("Bearer bfgO0LBH2XokPp9TALasYalJM8oIoAHw", forHTTPHeaderField: "Authorization")
        print(thermostatRequest.allHTTPHeaderFields)
        
        let parser = JsonParser()
        
        let dataTask = NSURLSession.sharedSession().dataTaskWithRequest(thermostatRequest) { (data, response, error) -> Void in
            
            if let error = error {
                print("Error: \(error.localizedDescription): \(error.userInfo)")
                return
            }
            
            if let httpResponse = response as? NSHTTPURLResponse {
                switch httpResponse.statusCode {
                case 200...299:
                    print("the right thing")
                case 401:
                    print("error thing")
                    print(response)
                default:
                    print("do some other thing")
                    print(response)
                }
                
            }
            
            guard let data = data else {
                return
            }
            
            if let json = parser.parseJSON(data) {
                
                if let _ = json["error"] {
                    //TODO: Handle Error
                    print(json)
                } else if let thermostatList = json["thermostatList"]?[0] {
                    print(thermostatList)
                    print("made it in")

                    self.thermostatOne.identifier = thermostatList["identifier"] as! String
                    self.thermostatOne.isRegistered = thermostatList["isRegistered"] as! Bool
                    self.thermostatOne.modelNumber = thermostatList["modelNumber"] as! String
                    self.thermostatOne.serialNumber = thermostatList["serialNumber"] as! String
                    self.thermostatOne.lastModified = thermostatList["lastModified"] as! String
                    self.thermostatOne.settings = thermostatList["settings"] as! [String:String]
                    
                    return
                }
                
                return
            }
        }
        
        dataTask.resume()
        
    }
    
}

func requestLogs() {
    //get the logs
}





